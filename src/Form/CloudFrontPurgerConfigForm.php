<?php

namespace Drupal\cloudfront_purger\Form;

use Drupal\Core\Form\ConfigTarget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\purge_ui\Form\PurgerConfigFormBase;

/**
 * Provides a config form for CloudFront purger.
 */
class CloudFrontPurgerConfigForm extends PurgerConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cloudfront_purger.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudfront_purger.config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Whether Cloudfront Purger is enabled.'),
      '#config_target' => new ConfigTarget(
        'cloudfront_purger.settings',
        'disabled',
        fn($value) => $value ? 0 : 1,
        fn($value) => !isset($value),
      ),
    ];
    $form['distribution_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Distribution ID'),
      '#config_target' => 'cloudfront_purger.settings:distribution_id',
    ];
    $form['aws_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS Key'),
      '#config_target' => 'cloudfront_purger.settings:aws_key',
    ];
    $form['aws_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS Secret'),
      '#config_target' => 'cloudfront_purger.settings:aws_secret',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state): void {
    // We need to manually save config as we are not a config form.
    $settings = $this->config('cloudfront_purger.settings');
    $settings->set('disabled', empty($form_state->getValue('enabled')));
    $settings->set('distribution_id', $form_state->getValue('distribution_id'));
    $settings->set('aws_key', $form_state->getValue('aws_key'));
    $settings->set('aws_secret', $form_state->getValue('aws_secret'));
    $settings->save();
  }

}
