<?php

declare(strict_types=1);

namespace Drupal\Tests\cloudfront_purger\Unit;

use Aws\CloudFront\CloudFrontClient;
use Aws\CommandInterface;
use Aws\Exception\AwsException;
use Aws\Result;
use Drupal\Tests\UnitTestCase;
use Drupal\cloudfront_purger\CloudFrontInvalidator;
use Psr\Log\LoggerInterface;

/**
 * Tests for CloudFrontInvalidator,.
 *
 * @coversDefaultClass \Drupal\cloudfront_purger\CloudFrontInvalidator
 * @group cloudfront_purger
 */
class CloudFrontInvalidatorTest extends UnitTestCase {

  /**
   * The invalidator under test.
   *
   * @var \Drupal\cloudfront_purger\CloudFrontInvalidator
   */
  protected $invalidator;

  /**
   * The CloudFront client.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Aws\CloudFront\CloudFrontClient
   */
  protected $client;

  /**
   * The logger.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->client = $this->createMock(CloudFrontClient::class);
    $this->invalidator = new CloudFrontInvalidator($this->client, $this->logger);
  }

  /**
   * Tests invalidate.
   *
   * @covers ::invalidate
   */
  public function testInvalidate() {

    $this->logger->expects($this->once())
      ->method('info');

    $result = new Result([
      'Invalidation' => ['Id' => 'ABCD1234'],
    ]);

    $this->client->expects($this->once())
      ->method('__call')
      ->with($this->equalTo('createInvalidation'))
      ->willReturn($result);

    $paths = ['node/1', 'blog/*', '/'];
    $distribution_id = 'ABCD1234';

    $id = $this->invalidator->invalidate($paths, $distribution_id);

    $this->assertEquals('ABCD1234', $id);
  }

  /**
   * Tests an error.
   *
   * @covers ::invalidate
   */
  public function testInvalidateError() {
    $this->expectException(AwsException::class);
    $this->logger->expects($this->never())
      ->method('info');

    /** @var \PHPUnit\Framework\MockObject\MockObject|\Aws\CommandInterface $command */
    $command = $this->createMock(CommandInterface::class);

    $this->client->expects($this->once())
      ->method('__call')
      ->with($this->equalTo('createInvalidation'))
      ->will($this->throwException(new AwsException('test message', $command)));

    $paths = ['node/1', 'blog/*', '/'];
    $distribution_id = 'ABCD1234';

    $validationId = $this->invalidator->invalidate($paths, $distribution_id);

    $this->assertEmpty($validationId);

  }

}
