<?php

declare(strict_types=1);

namespace Drupal\Tests\cloudfront_purger\Kernel\Form;

use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase;
use Drupal\cloudfront_purger\Form\CloudFrontPurgerConfigForm;

/**
 * Tests for CloudFrontPurgerConfigForm.
 *
 * @coversDefaultClass \Drupal\cloudfront_purger\Form\CloudFrontPurgerConfigForm
 * @group cloudfront_purger
 */
class CloudFrontPurgerConfigFormTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['purge', 'purge_ui', 'cloudfront_purger'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig(['purge', 'purge_ui', 'cloudfront_purger']);
  }

  /**
   * Tests that submitted values are processed and saved correctly.
   */
  public function testConfigValuesSavedCorrectly(): void {
    // cspell:disable
    $formState = new FormState();
    $formState->setValues([
      'enabled' => 'enabled',
      'distribution_id' => 'ABCDEF12345',
      'aws_key' => 'GHIJKL1234',
      'aws_secret' => 'XYZabc123456789',
    ]);
    $formState->addBuildInfo('args', [['dialog' => FALSE]]);
    $this->container->get('form_builder')->submitForm(
      CloudFrontPurgerConfigForm::class,
      $formState
    );
    $config = $this->config('cloudfront_purger.settings');
    $this->assertFalse($config->get('disabled'));
    $this->assertEquals('ABCDEF12345', $config->get('distribution_id'));
    $this->assertEquals('GHIJKL1234', $config->get('aws_key'));
    $this->assertEquals('XYZabc123456789', $config->get('aws_secret'));
    // cspell:enable
  }

}
